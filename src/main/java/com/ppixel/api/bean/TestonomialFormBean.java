package com.ppixel.api.bean;

import java.util.Date;

public class TestonomialFormBean {
	
	private Integer autoId;
	private String testonomial;
	private String author;
	private String position;
	private String imageUrl;
	private String batchId;
	private String userId;
	private Date createdDate;
	private Date updatedDate;
	private String status;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAutoId() {
		return autoId;
	}
	
	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}
	
	public String getTestonomial() {
		return testonomial;
	}
	
	public void setTestonomial(String testonomial) {
		this.testonomial = testonomial;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public String getBatchId() {
		return batchId;
	}
	
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}
	
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

}