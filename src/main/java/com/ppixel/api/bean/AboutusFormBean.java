package com.ppixel.api.bean;

public class AboutusFormBean {

	private String autoId;
	private String aboutTitle;
	private String userId;
	private String aboutusContent;
	private String aboutusImage;
	private String createdDate;
	private String updatedDate;
	private String status;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAutoId() {
		return autoId;
	}

	public void setAutoId(String autoId) {
		this.autoId = autoId;
	}

	public String getAboutTitle() {
		return aboutTitle;
	}

	public void setAboutTitle(String aboutTitle) {
		this.aboutTitle = aboutTitle;
	}

	public String getAboutusContent() {
		return aboutusContent;
	}

	public void setAboutusContent(String aboutusContent) {
		this.aboutusContent = aboutusContent;
	}

	public String getAboutusImage() {
		return aboutusImage;
	}

	public void setAboutusImage(String aboutusImage) {
		this.aboutusImage = aboutusImage;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
