package com.ppixel.api.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TESTONOMIAL_MASTER")
public class TestonomialMasterBean {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUTO_ID", columnDefinition = "int(20)")
	private Integer autoId;

	@Column(name = "TESTOOMIAL", columnDefinition = "TEXT")
	private String testonomial;

	@Column(name = "AUTHOR", columnDefinition = "VARCHAR(300)")
	private String author;

	@Column(name = "POSITION", columnDefinition = "VARCHAR(300)")
	private String position;

	
	@Column(name = "USER_ID",columnDefinition = "VARCHAR(20)")
	private String userId;
	
	@Column(name = "IMAGE_URL", columnDefinition = "VARCHAR(20)")
	private String imageUrl;
	
	@Column(name = "BATCH_ID", columnDefinition = "VARCHAR(20)")
	private String batchId;

	@Column(name = "CREATED_DATE", columnDefinition = "DATETIME")
	private Date createdDate;

	@Column(name = "UPDATED_DATE", columnDefinition = "DATETIME")
	private Date updatedDate;

	@Column(name = "STATUS", columnDefinition = "VARCHAR(20)")
	private String status;

	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getTestonomial() {
		return testonomial;
	}

	public void setTestonomial(String testonomial) {
		this.testonomial = testonomial;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
