package com.ppixel.api.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BANNER_MASTER")
public class BannerMasterBean {

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUTO_ID",columnDefinition = "int(10)")
	private Integer autoId;
	
	@Column(name = "BANNER_TITLE",columnDefinition = "VARCHAR(500)")
	private String bannerTitle;
	
	@Column(name = "BANNER_CONTENT_1",columnDefinition = "TEXT")
	private String bannerContent1;
	
	@Column(name = "USER_ID",columnDefinition = "VARCHAR(20)")
	private String userId;
	
	@Column(name = "BANNER_CONTENT_2",columnDefinition = "TEXT")
	private String bannerContent2;
	
	@Column(name = "BANNER_IMAGE",columnDefinition = "TEXT")
	private String bannerImage;
	
	@Column(name = "BANNER_LINK_URL",columnDefinition = "VARCHAR(4000)")
	private String bannerLinkUrl;
	
	@Column(name = "BANNER_POSITION",columnDefinition = "VARCHAR(400)")
	private String bannerPosition;
	
	@Column(name = "BANNER_ORDER",columnDefinition = "VARCHAR(400)")
	private String bannerOrder;
	
	@Column(name = "CREATED_DATE",columnDefinition = "DATETIME")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE",columnDefinition = "DATETIME")
	private Date updatedDate;
	
	@Column(name = "STATUS",columnDefinition = "VARCHAR(20)")
	private String status;

	
	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getBannerTitle() {
		return bannerTitle;
	}

	public void setBannerTitle(String bannerTitle) {
		this.bannerTitle = bannerTitle;
	}

	

	public String getBannerContent1() {
		return bannerContent1;
	}

	public void setBannerContent1(String bannerContent1) {
		this.bannerContent1 = bannerContent1;
	}

	public String getBannerContent2() {
		return bannerContent2;
	}

	public void setBannerContent2(String bannerContent2) {
		this.bannerContent2 = bannerContent2;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public String getBannerLinkUrl() {
		return bannerLinkUrl;
	}

	public void setBannerLinkUrl(String bannerLinkUrl) {
		this.bannerLinkUrl = bannerLinkUrl;
	}

	public String getBannerPosition() {
		return bannerPosition;
	}

	public void setBannerPosition(String bannerPosition) {
		this.bannerPosition = bannerPosition;
	}

	public String getBannerOrder() {
		return bannerOrder;
	}

	public void setBannerOrder(String bannerOrder) {
		this.bannerOrder = bannerOrder;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
}
