package com.ppixel.api.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="COURSE_MASTER")
public class CourseMasterBean {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUTO_ID",columnDefinition = "int(20)")
	private Integer autoId;
	
	@Column(name = "COURSE_TITLE",columnDefinition = "VARCHAR(200)")
	private String courseTitle;
	
	@Column(name = "DESCRIPTION",columnDefinition = "VARCHAR(200)")
	private String description;
	
	@Column(name = "USER_ID",columnDefinition = "VARCHAR(20)")
	private String userId;
	
	@Column(name = "INSTRUCTOR",columnDefinition = "VARCHAR(200)")
	private String instructor;
	
	@Column(name = "PRICE",columnDefinition = "DOUBLE")
	private Double price;
	
	@Column(name = "CREATED_DATE",columnDefinition = "DATETIME")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE",columnDefinition = "DATETIME")
	private Date updatedDate;
	
	@Column(name = "STATUS",columnDefinition = "VARCHAR(20)")
	private String status;

	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInstructor() {
		return instructor;
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
