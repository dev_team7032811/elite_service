package com.ppixel.api.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USER_ADMN_DETAILS")
public class UserAdminMasterBean {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUTO_ID", columnDefinition = "int(20)")
	private Integer autoId;

	@Column(name = "USER_NAME", columnDefinition = "VARCHAR(300)")
	private String userName;

	@Column(name = "EMAIL_ID", columnDefinition = "VARCHAR(300)")
	private String emailId;

	@Column(name = "PASSWORD", columnDefinition = "VARCHAR(300)")
	private String password;

	@Column(name = "MOBILE_NUMBER", columnDefinition = "VARCHAR(30)")
	private String mobileNumber;
	
	@Column(name = "USER_ID",columnDefinition = "VARCHAR(20)")
	private String userId;

	@Column(name = "CREATED_DATE", columnDefinition = "DATETIME")
	private Date createdDate;

	@Column(name = "UPDATED_DATE", columnDefinition = "DATETIME")
	private Date updatedDate;

	@Column(name = "STATUS", columnDefinition = "VARCHAR(20)")
	private String status;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
