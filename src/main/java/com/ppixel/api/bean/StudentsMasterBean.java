package com.ppixel.api.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STUDENTS_MASTER")
public class StudentsMasterBean {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUTO_ID", columnDefinition = "int(20)")
	private Integer autoId;

	@Column(name = "NAME", columnDefinition = "VARCHAR(300)")
	private String name;

	@Column(name = "MOBILE_NUMBER", columnDefinition = "VARCHAR(300)")
	private String mobileNumber;
	
	@Column(name = "USER_ID",columnDefinition = "VARCHAR(20)")
	private String userId;

	@Column(name = "EMAIL_ID", columnDefinition = "VARCHAR(300)")
	private String emailId;

	@Column(name = "COURSE_ID", columnDefinition = "VARCHAR(20)")
	private String courseId;
	
	@Column(name = "BATCH_ID", columnDefinition = "VARCHAR(20)")
	private String batchId;

	@Column(name = "CREATED_DATE", columnDefinition = "DATETIME")
	private Date createdDate;

	@Column(name = "UPDATED_DATE", columnDefinition = "DATETIME")
	private Date updatedDate;

	@Column(name = "STATUS", columnDefinition = "VARCHAR(20)")
	private String status;

	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
