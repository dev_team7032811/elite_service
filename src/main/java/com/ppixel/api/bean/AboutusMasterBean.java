package com.ppixel.api.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ABOUTUS_MASTER")
public class AboutusMasterBean {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUTO_ID",columnDefinition = "int(10)")
	private Integer autoId;
	
	@Column(name = "ABOUTUS_TITLE",columnDefinition = "VARCHAR(500)")
	private String aboutTitle;
	
	@Column(name = "ABOUTUS_CONTENT",columnDefinition = "TEXT")
	private String aboutusContent;
	
	@Column(name = "USER_ID",columnDefinition = "VARCHAR(20)")
	private String userId;
	
	@Column(name = "ABOUTUS_IMAGE",columnDefinition = "VARCHAR(10000)")
	private String aboutusImage;
	
	@Column(name = "CREATED_DATE",columnDefinition = "DATETIME")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE",columnDefinition = "DATETIME")
	private Date updatedDate;
	
	@Column(name = "STATUS",columnDefinition = "VARCHAR(20)")
	private String status;

	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getAboutTitle() {
		return aboutTitle;
	}

	public void setAboutTitle(String aboutTitle) {
		this.aboutTitle = aboutTitle;
	}


	public String getAboutusContent() {
		return aboutusContent;
	}

	public void setAboutusContent(String aboutusContent) {
		this.aboutusContent = aboutusContent;
	}

	public String getAboutusImage() {
		return aboutusImage;
	}

	public void setAboutusImage(String aboutusImage) {
		this.aboutusImage = aboutusImage;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
}
