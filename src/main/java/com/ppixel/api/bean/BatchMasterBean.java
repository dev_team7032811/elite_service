package com.ppixel.api.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BATCH_MASTER")
public class BatchMasterBean {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUTO_ID",columnDefinition = "int(20)")
	private Integer autoId;
	
	@Column(name = "BATCH_NAME",columnDefinition = "VARCHAR(200)")
	private String batchName;
	
	@Column(name = "DESCRIPTION",columnDefinition = "VARCHAR(200)")
	private String description;
	
	@Column(name = "START_DATE",columnDefinition = "DATETIME")
	private Date startDate;
	
	@Column(name = "USER_ID",columnDefinition = "VARCHAR(20)")
	private String userId;
	
	@Column(name = "END_DATE",columnDefinition = "DATETIME")
	private Date endDate;
	
	@Column(name = "CREATED_DATE",columnDefinition = "DATETIME")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE",columnDefinition = "DATETIME")
	private Date updatedDate;
	
	@Column(name = "COURSE_ID",columnDefinition = "int(20)")
	private Integer courseId;
	
	@Column(name = "STATUS",columnDefinition = "VARCHAR(20)")
	private String status;

	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}