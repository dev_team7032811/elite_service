package com.ppixel.api.bean;

public class BannerFormBean {

	private String autoId;
	private String bannerTitle;
	private String userId;
	private String bannerContent1;
	private String bannerContent2;
	private String bannerImage;
	private String bannerLinkUrl;
	private String bannerPosition;
	private String bannerOrder;
	private String createdDate;
	private String updatedDate;
	private String status;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAutoId() {
		return autoId;
	}

	public void setAutoId(String autoId) {
		this.autoId = autoId;
	}

	public String getBannerTitle() {
		return bannerTitle;
	}

	public void setBannerTitle(String bannerTitle) {
		this.bannerTitle = bannerTitle;
	}

	public String getBannerContent1() {
		return bannerContent1;
	}

	public void setBannerContent1(String bannerContent1) {
		this.bannerContent1 = bannerContent1;
	}

	public String getBannerContent2() {
		return bannerContent2;
	}

	public void setBannerContent2(String bannerContent2) {
		this.bannerContent2 = bannerContent2;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public String getBannerLinkUrl() {
		return bannerLinkUrl;
	}

	public void setBannerLinkUrl(String bannerLinkUrl) {
		this.bannerLinkUrl = bannerLinkUrl;
	}

	public String getBannerPosition() {
		return bannerPosition;
	}

	public void setBannerPosition(String bannerPosition) {
		this.bannerPosition = bannerPosition;
	}

	public String getBannerOrder() {
		return bannerOrder;
	}

	public void setBannerOrder(String bannerOrder) {
		this.bannerOrder = bannerOrder;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
