package com.ppixel.api.wrapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.bean.AboutusFormBean;
import com.ppixel.api.service.AboutusService;

@Service
public class AboutusWrapper {

	@Autowired
	AboutusService aboutusService;

	public ResponseDetails saveAndUpdateAboutus(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = aboutusService.saveAndUpdateAboutus(requestDetails.getAboutusFormBean());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Aboutus Save Successfully");

			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Aboutus Not Save Successfully");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getAboutusList(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			List<AboutusFormBean> aboutusFormBeans = aboutusService.getAboutusList();
			if (aboutusFormBeans.size() > 0) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Aboutus Data Found");
				responseDetails.setAboutusFormBeans(aboutusFormBeans);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Aboutus Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getAboutusById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			AboutusFormBean aboutusFormBean = aboutusService.getAboutusById(requestDetails.getId());
			if (aboutusFormBean != null) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Aboutus Data Found");
				responseDetails.setAboutusFormBean(aboutusFormBean);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Aboutus Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails deleteAboutusById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = aboutusService.deleteAboutusById(requestDetails.getId());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Aboutus Data Found");
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Aboutus Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

}
