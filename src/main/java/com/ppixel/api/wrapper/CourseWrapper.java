package com.ppixel.api.wrapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.bean.CourseFormBean;
import com.ppixel.api.service.CourseService;

@Service
public class CourseWrapper {

	@Autowired
	CourseService courseService;

	public ResponseDetails saveAndUpdateCourseDetails(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = courseService.saveAndUpdateCourseDetails(requestDetails.getCourseFormBean());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Course Save Successfully");

			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Course Not Save Successfully");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getCourseList(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			List<CourseFormBean> courseFormBeans = courseService.getCourseList();
			if (courseFormBeans.size() > 0) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Course Data Found");
				responseDetails.setCourseFormBeans(courseFormBeans);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Course Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getCourseDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			CourseFormBean courseFormBean = courseService.getCourseDetailsById(requestDetails.getId());
			if (courseFormBean != null) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Course Data Found");
				responseDetails.setCourseFormBean(courseFormBean);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Course Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}
	
	public ResponseDetails deleteCourseDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = courseService.deleteCourseDetailsById(requestDetails.getId());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Course Data Found");
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Course Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

}
