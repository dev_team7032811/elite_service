package com.ppixel.api.wrapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.bean.StudentsFormBean;
import com.ppixel.api.service.StudentsService;

@Service
public class StudentsWrapper {

	@Autowired
	StudentsService studentsService;

	public ResponseDetails saveAndUpdateStudentsDetails(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = studentsService.saveAndUpdateStudentsDetails(requestDetails.getStudentsFormBean());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Course Save Successfully");

			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Course Not Save Successfully");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getStudentsList(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			List<StudentsFormBean> studentsFormBeans = studentsService.getStudentsList();
			if (studentsFormBeans.size() > 0) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Students Data Found");
				responseDetails.setStudentsFormBeans(studentsFormBeans);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Students Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getStudentsDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			StudentsFormBean studentsFormBean = studentsService.getStudentsDetailsById(requestDetails.getId());
			if (studentsFormBean != null) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Students Data Found");
				responseDetails.setStudentsFormBean(studentsFormBean);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Students Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails deleteStudentsDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = studentsService.deleteStudentsDetailsById(requestDetails.getId());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Students Data Found");
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Students Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

}
