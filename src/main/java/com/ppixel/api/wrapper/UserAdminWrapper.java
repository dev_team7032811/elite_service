package com.ppixel.api.wrapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.bean.UserAdminFormBean;
import com.ppixel.api.service.UserAdminService;

@Service
public class UserAdminWrapper {

	
	@Autowired
	UserAdminService userAdminService;
	
	
	public ResponseDetails getUserAdminDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			UserAdminFormBean userAdminFormBean = userAdminService.getUserAdminDetailsById(requestDetails.getUserAdminFormBean());
			if (userAdminFormBean!=null) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Course Data Found");
				responseDetails.setUserAdminFormBean(userAdminFormBean);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Course Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}
}
