package com.ppixel.api.wrapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.bean.BannerFormBean;
import com.ppixel.api.service.BannerService;

@Service
public class BannerWrapper {

	@Autowired
	BannerService bannerService;

	public ResponseDetails saveAndUpdateBannerDetails(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = bannerService.saveAndUpdateBannerDetails(requestDetails.getBannerFormBean());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Banner Save Successfully");

			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Banner Not Save Successfully");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getBannerList(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			List<BannerFormBean> bannerFormBeans = bannerService.getBannerList();
			if (bannerFormBeans.size() > 0) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Banner Data Found");
				responseDetails.setBannerFormBeans(bannerFormBeans);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Banner Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getBannerDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			BannerFormBean bannerFormBean = bannerService.getBannerDetailsById(requestDetails.getId());
			if (bannerFormBean != null) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Banner Data Found");
				responseDetails.setBannerFormBean(bannerFormBean);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Banner Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails deleteBannerDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = bannerService.deleteBannerDetailsById(requestDetails.getId());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Banner Data Found");
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Banner Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

}
