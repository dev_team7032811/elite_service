package com.ppixel.api.wrapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.bean.BatchFormBean;
import com.ppixel.api.service.BatchService;

@Service
public class BatchWrapper {
	
	@Autowired
	BatchService batchService;

	public ResponseDetails saveAndUpdateBatchDetails(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = batchService.saveAndUpdateBatchDetails(requestDetails.getBatchFormBean());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Batch Details Saved Successfully!!");

			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Batch Details Not Saved Successfully");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getBatchList(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			List<BatchFormBean> batchFormBeans = batchService.getBatchList();
			if (batchFormBeans.size() > 0) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Batch Data Found");
				responseDetails.setBatchFormBeans(batchFormBeans);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Batch Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getBatchDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			BatchFormBean batchFormBean = batchService.getBatchDetailsById(requestDetails.getId());
			if (batchFormBean != null) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Batch Data Found");
				responseDetails.setBatchFormBean(batchFormBean);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Batch Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}
	
	public ResponseDetails deleteBatchDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = batchService.deleteBatchDetailsById(requestDetails.getId());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Batch Data Found");
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Batch Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

}