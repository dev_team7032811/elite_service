package com.ppixel.api.wrapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.bean.TestonomialFormBean;
import com.ppixel.api.service.TestonomialService;

@Service
public class TestonomialWrapper {

	@Autowired
	private TestonomialService testonomialService;

	public ResponseDetails saveOrUpdateTestonomialDetails(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = testonomialService.saveOrUpdateTestonomialDetails(requestDetails.getTestonomialFormBean());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Testonomial Details Saved Successfully!!");

			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Testonomial Details Not Saved Successfully");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getAllTestonomialDetailsList(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			List<TestonomialFormBean> testonomialFormBeans = testonomialService.getAllTestonomialList();
			if (testonomialFormBeans.size() > 0) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Testonomial Data Found");
				responseDetails.setTestonomialFormBeans(testonomialFormBeans);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Testonomial Data Not Found");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails getTestonomialDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			TestonomialFormBean testonomialFormBean = testonomialService
					.getTestonomialDetailsById(requestDetails.getId());
			if (testonomialFormBean != null) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Testonomial Data Found");
				responseDetails.setTestonomialFormBean(testonomialFormBean);
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Testonomial Data Not Found");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

	public ResponseDetails deleteTestonomialDetailsById(RequestDetails requestDetails) {
		ResponseDetails responseDetails = new ResponseDetails();
		try {
			Boolean result = testonomialService.deleteTestonomialDetailsById(requestDetails.getId());
			if (result) {
				responseDetails.setResponseCode("00");
				responseDetails.setResponseMessage("Testonomial Data Found");
			} else {
				responseDetails.setResponseCode("01");
				responseDetails.setResponseMessage("Testonomial Data Not Found");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return responseDetails;
	}

}