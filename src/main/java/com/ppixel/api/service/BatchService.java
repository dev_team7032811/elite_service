package com.ppixel.api.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.api.bean.BatchFormBean;
import com.ppixel.api.bean.BatchMasterBean;
import com.ppixel.api.dao.BatchDao;

@Service
public class BatchService {

	@Autowired
	BatchDao batchDao;

	public Boolean saveAndUpdateBatchDetails(BatchFormBean batchFormBean) throws Exception {
		Boolean result = false;
		try {
			BatchMasterBean batchMasterBean = new BatchMasterBean();
			if (batchFormBean.getAutoId() != null) {
				batchMasterBean.setAutoId(Integer.parseInt(batchFormBean.getAutoId()));
				batchMasterBean.setUpdatedDate(new Date());
			} else {
				batchMasterBean.setCreatedDate(new Date());

			}
			batchMasterBean.setBatchName(batchFormBean.getBatchName());
			if (batchFormBean.getStartDate() != null) {
				batchMasterBean.setStartDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(batchFormBean.getStartDate()));
			}
			if (batchFormBean.getEndDate() != null) {
				batchMasterBean.setEndDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(batchFormBean.getEndDate()));
			}

			batchMasterBean.setDescription(batchFormBean.getDescription());
			batchMasterBean.setUserId(batchFormBean.getUserId());
			//batchMasterBean.setCourseId(Integer.parseInt(batchFormBean.getCourseId()));
			batchMasterBean.setStatus("A");
			batchDao.save(batchMasterBean);
			result = true;

		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	public List<BatchFormBean> getBatchList() throws Exception {
		List<BatchFormBean> batchFormBeans = new ArrayList<BatchFormBean>();

		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			List<BatchMasterBean> batchMasterBeans = batchDao.findBatchList();
			for (BatchMasterBean batchMasterBean : batchMasterBeans) {
				BatchFormBean batchFormBean = new BatchFormBean();
				batchFormBean
						.setAutoId(null != batchMasterBean.getAutoId() ? batchMasterBean.getAutoId().toString() : null);
				batchFormBean.setUpdatedDate(
						null != batchMasterBean.getUpdatedDate() ? dateFormat.format(batchMasterBean.getUpdatedDate())
								: null);
				batchFormBean.setCreatedDate(
						null != batchMasterBean.getCreatedDate() ? dateFormat.format(batchMasterBean.getCreatedDate())
								: null);
				batchFormBean.setStartDate(
						null != batchMasterBean.getStartDate() ? dateFormat.format(batchMasterBean.getStartDate())
								: null);
				batchFormBean.setEndDate(
						null != batchMasterBean.getEndDate() ? dateFormat.format(batchMasterBean.getEndDate()) : null);
				batchFormBean.setBatchName(batchMasterBean.getBatchName());
				batchFormBean.setDescription(batchMasterBean.getDescription());
				batchFormBean.setCourseId(
						null != batchMasterBean.getCourseId() ? batchMasterBean.getCourseId().toString() : null);
				batchFormBean.setStatus("A");
				batchFormBeans.add(batchFormBean);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return batchFormBeans;
	}

	public BatchFormBean getBatchDetailsById(String autoId) throws Exception {
		BatchFormBean batchFormBean = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			BatchMasterBean batchMasterBean = batchDao.findBatchById(Integer.parseInt(autoId));
			if (batchMasterBean != null) {

				batchFormBean = new BatchFormBean();
				batchFormBean
						.setAutoId(null != batchMasterBean.getAutoId() ? batchMasterBean.getAutoId().toString() : null);
				batchFormBean.setUpdatedDate(
						null != batchMasterBean.getUpdatedDate() ? dateFormat.format(batchMasterBean.getUpdatedDate())
								: null);
				batchFormBean.setCreatedDate(
						null != batchMasterBean.getCreatedDate() ? dateFormat.format(batchMasterBean.getCreatedDate())
								: null);
				batchFormBean.setStartDate(
						null != batchMasterBean.getStartDate() ? dateFormat.format(batchMasterBean.getStartDate())
								: null);
				batchFormBean.setEndDate(
						null != batchMasterBean.getEndDate() ? dateFormat.format(batchMasterBean.getEndDate()) : null);
				batchFormBean.setBatchName(batchMasterBean.getBatchName());
				batchFormBean.setDescription(batchMasterBean.getDescription());
				batchFormBean.setCourseId(
						null != batchMasterBean.getCourseId() ? batchMasterBean.getCourseId().toString() : null);
				batchFormBean.setStatus("A");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return batchFormBean;
	}

	public Boolean deleteBatchDetailsById(String autoId) throws Exception {
		Boolean result = null;
		try {
			Integer id = batchDao.deleteBatchById(Integer.parseInt(autoId));

			if (id != 0) {
				result = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

}