package com.ppixel.api.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.api.bean.AboutusFormBean;
import com.ppixel.api.bean.AboutusMasterBean;
import com.ppixel.api.dao.AboutusDao;

@Service
public class AboutusService {

	@Autowired
	AboutusDao aboutusDao;

	public Boolean saveAndUpdateAboutus(AboutusFormBean aboutusFormBean) throws Exception {
		Boolean result = false;
		try {
			AboutusMasterBean aboutusMasterBean = new AboutusMasterBean();
			if (aboutusFormBean.getAutoId() != null) {
				aboutusMasterBean.setAutoId(Integer.parseInt(aboutusFormBean.getAutoId()));
				aboutusMasterBean.setUpdatedDate(new Date());
			} else {
				// aboutusMasterBean.setCreatedDate(new Date());

			}
			aboutusMasterBean.setCreatedDate(new Date());
			aboutusMasterBean.setAboutTitle(aboutusFormBean.getAboutTitle());
			aboutusMasterBean.setAboutusContent(aboutusFormBean.getAboutusContent());
			aboutusMasterBean.setAboutusImage(aboutusFormBean.getAboutusImage());
			aboutusMasterBean.setUserId(aboutusFormBean.getUserId());
			aboutusMasterBean.setStatus("A");
			aboutusDao.save(aboutusMasterBean);
			result = true;

		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	public List<AboutusFormBean> getAboutusList() throws Exception {
		List<AboutusFormBean> aboutusFormBeans = new ArrayList<AboutusFormBean>();

		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			List<AboutusMasterBean> aboutusMasterBeans = aboutusDao.findAboutusList();
			for (AboutusMasterBean aboutusMasterBean : aboutusMasterBeans) {
				AboutusFormBean aboutusFormBean = new AboutusFormBean();
				aboutusFormBean.setAutoId(
						null != aboutusMasterBean.getAutoId() ? aboutusMasterBean.getAutoId().toString() : null);
				aboutusFormBean.setUpdatedDate(null != aboutusMasterBean.getUpdatedDate()
						? dateFormat.format(aboutusMasterBean.getUpdatedDate())
						: null);
				aboutusFormBean.setCreatedDate(null != aboutusMasterBean.getCreatedDate()
						? dateFormat.format(aboutusMasterBean.getCreatedDate())
						: null);
				aboutusFormBean.setAboutTitle(aboutusMasterBean.getAboutTitle());
				aboutusFormBean.setAboutusContent(aboutusMasterBean.getAboutusContent());
				aboutusFormBean.setAboutusImage(aboutusMasterBean.getAboutusImage());
				aboutusFormBean.setStatus(aboutusMasterBean.getStatus());
				aboutusFormBeans.add(aboutusFormBean);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return aboutusFormBeans;
	}

	public AboutusFormBean getAboutusById(String autoId) throws Exception {
		AboutusFormBean aboutusFormBean = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			AboutusMasterBean aboutusMasterBean = aboutusDao.findAboutusById(Integer.parseInt(autoId));
			if (aboutusMasterBean != null) {

				aboutusFormBean = new AboutusFormBean();
				aboutusFormBean.setAutoId(
						null != aboutusMasterBean.getAutoId() ? aboutusMasterBean.getAutoId().toString() : null);
				aboutusFormBean.setUpdatedDate(null != aboutusMasterBean.getUpdatedDate()
						? dateFormat.format(aboutusMasterBean.getUpdatedDate())
						: null);
				aboutusFormBean.setCreatedDate(null != aboutusMasterBean.getCreatedDate()
						? dateFormat.format(aboutusMasterBean.getCreatedDate())
						: null);
				aboutusFormBean.setAboutTitle(aboutusMasterBean.getAboutTitle());
				aboutusFormBean.setAboutusContent(aboutusMasterBean.getAboutusContent());
				aboutusFormBean.setAboutusImage(aboutusMasterBean.getAboutusImage());
				aboutusFormBean.setStatus(aboutusMasterBean.getStatus());

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return aboutusFormBean;
	}

	public Boolean deleteAboutusById(String autoId) throws Exception {
		Boolean result = null;
		try {
			Integer id = aboutusDao.deleteAboutusById(Integer.parseInt(autoId));

			if (id != 0) {
				result = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

}
