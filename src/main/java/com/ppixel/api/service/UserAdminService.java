package com.ppixel.api.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.api.bean.UserAdminFormBean;
import com.ppixel.api.bean.UserAdminMasterBean;
import com.ppixel.api.dao.UserAdminDao;

@Service
public class UserAdminService {

	@Autowired
	UserAdminDao userAdminDao;

	public UserAdminFormBean getUserAdminDetailsById(UserAdminFormBean userAdminFormBean) throws Exception {
		UserAdminFormBean userAdminFormBean1 = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			UserAdminMasterBean userAdminMasterBean = userAdminDao
					.validateEmailAndPassword(userAdminFormBean.getEmailId(), userAdminFormBean.getPassword());
			if (userAdminMasterBean != null) {

				userAdminFormBean1 = new UserAdminFormBean();
				userAdminFormBean1.setAutoId(
						null != userAdminMasterBean.getAutoId() ? userAdminMasterBean.getAutoId().toString() : null);
				userAdminFormBean1.setUpdatedDate(null != userAdminMasterBean.getUpdatedDate()
						? dateFormat.format(userAdminMasterBean.getUpdatedDate())
						: null);
				userAdminFormBean1.setCreatedDate(null != userAdminMasterBean.getCreatedDate()
						? dateFormat.format(userAdminMasterBean.getCreatedDate())
						: null);
				userAdminFormBean1.setEmailId(userAdminMasterBean.getEmailId());
				userAdminFormBean1.setPassword(userAdminMasterBean.getPassword());
				userAdminFormBean1.setEmailId(userAdminMasterBean.getEmailId());
				userAdminFormBean1.setMobileNumber(userAdminMasterBean.getMobileNumber());
				userAdminFormBean1.setUserName(userAdminMasterBean.getUserName());
				userAdminFormBean1.setStatus(userAdminMasterBean.getStatus());

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return userAdminFormBean1;
	}

}
