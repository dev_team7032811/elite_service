package com.ppixel.api.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.api.bean.StudentsFormBean;
import com.ppixel.api.bean.StudentsMasterBean;
import com.ppixel.api.dao.StudentsDao;

@Service
public class StudentsService {

	@Autowired
	StudentsDao studentsDao;

	public Boolean saveAndUpdateStudentsDetails(StudentsFormBean studentsFormBean) throws Exception {
		Boolean result = false;
		try {
			StudentsMasterBean studentsMasterBean = new StudentsMasterBean();
			if (studentsFormBean.getAutoId() != null) {
				studentsMasterBean.setAutoId(Integer.parseInt(studentsFormBean.getAutoId()));
				studentsMasterBean.setUpdatedDate(new Date());
			} else {
				// studentsMasterBean.setCreatedDate(new Date());

			}
			studentsMasterBean.setCreatedDate(new Date());
			studentsMasterBean.setBatchId(studentsFormBean.getBatchId());
			studentsMasterBean.setCourseId(studentsFormBean.getCourseId());
			studentsMasterBean.setUserId(studentsFormBean.getUserId());
			studentsMasterBean.setEmailId(studentsFormBean.getEmailId());
			studentsMasterBean.setMobileNumber(studentsFormBean.getMobileNumber());
			studentsMasterBean.setName(studentsFormBean.getName());
			studentsMasterBean.setStatus("A");
			studentsDao.save(studentsMasterBean);
			result = true;

		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	public List<StudentsFormBean> getStudentsList() throws Exception {
		List<StudentsFormBean> studentsFormBeans = new ArrayList<StudentsFormBean>();

		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			List<StudentsMasterBean> studentsMasterBeans = studentsDao.findStudentsList();
			for (StudentsMasterBean studentsMasterBean : studentsMasterBeans) {
				StudentsFormBean studentsFormBean = new StudentsFormBean();
				studentsFormBean.setAutoId(
						null != studentsMasterBean.getAutoId() ? studentsMasterBean.getAutoId().toString() : null);
				studentsFormBean.setUpdatedDate(null != studentsMasterBean.getUpdatedDate()
						? dateFormat.format(studentsMasterBean.getUpdatedDate())
						: null);
				studentsFormBean.setCreatedDate(null != studentsMasterBean.getCreatedDate()
						? dateFormat.format(studentsMasterBean.getCreatedDate())
						: null);
				studentsFormBean.setBatchId(studentsMasterBean.getBatchId());
				studentsFormBean.setCourseId(studentsMasterBean.getCourseId());
				studentsFormBean.setEmailId(studentsMasterBean.getEmailId());
				studentsFormBean.setMobileNumber(studentsMasterBean.getMobileNumber());
				studentsFormBean.setName(studentsMasterBean.getName());
				studentsFormBean.setStatus(studentsMasterBean.getStatus());
				studentsFormBeans.add(studentsFormBean);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return studentsFormBeans;
	}

	public StudentsFormBean getStudentsDetailsById(String autoId) throws Exception {
		StudentsFormBean studentsFormBean = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			StudentsMasterBean studentsMasterBean = studentsDao.findStudentsById(Integer.parseInt(autoId));
			if (studentsMasterBean != null) {

				studentsFormBean = new StudentsFormBean();
				studentsFormBean.setAutoId(
						null != studentsMasterBean.getAutoId() ? studentsMasterBean.getAutoId().toString() : null);
				studentsFormBean.setUpdatedDate(null != studentsMasterBean.getUpdatedDate()
						? dateFormat.format(studentsMasterBean.getUpdatedDate())
						: null);
				studentsFormBean.setCreatedDate(null != studentsMasterBean.getCreatedDate()
						? dateFormat.format(studentsMasterBean.getCreatedDate())
						: null);
				studentsFormBean.setBatchId(studentsMasterBean.getBatchId());
				studentsFormBean.setCourseId(studentsMasterBean.getCourseId());
				studentsFormBean.setEmailId(studentsMasterBean.getEmailId());
				studentsFormBean.setMobileNumber(studentsMasterBean.getMobileNumber());
				studentsFormBean.setName(studentsMasterBean.getName());
				studentsFormBean.setStatus(studentsMasterBean.getStatus());

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return studentsFormBean;
	}

	public Boolean deleteStudentsDetailsById(String autoId) throws Exception {
		Boolean result = null;
		try {
			Integer id = studentsDao.deleteStudentsById(Integer.parseInt(autoId));

			if (id != 0) {
				result = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

}
