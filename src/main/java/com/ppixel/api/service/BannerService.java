package com.ppixel.api.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.api.bean.BannerFormBean;
import com.ppixel.api.bean.BannerMasterBean;
import com.ppixel.api.dao.BannerDao;

@Service
public class BannerService {

	@Autowired
	BannerDao bannerDao;

	public Boolean saveAndUpdateBannerDetails(BannerFormBean bannerFormBean) throws Exception {
		Boolean result = false;
		try {
			BannerMasterBean bannerMasterBean = new BannerMasterBean();
			if (bannerFormBean.getAutoId() != null) {
				bannerMasterBean.setAutoId(Integer.parseInt(bannerFormBean.getAutoId()));
				bannerMasterBean.setUpdatedDate(new Date());
			} else {
				// bannerMasterBean.setCreatedDate(new Date());

			}
			bannerMasterBean.setBannerContent1(bannerFormBean.getBannerContent1());
			bannerMasterBean.setBannerContent2(bannerFormBean.getBannerContent2());
			bannerMasterBean.setBannerImage(bannerFormBean.getBannerImage());
			bannerMasterBean.setBannerLinkUrl(bannerFormBean.getBannerLinkUrl());
			bannerMasterBean.setBannerOrder(bannerFormBean.getBannerOrder());
			bannerMasterBean.setBannerPosition(bannerFormBean.getBannerPosition());
			bannerMasterBean.setBannerTitle(bannerFormBean.getBannerTitle());
			bannerMasterBean.setUserId(bannerFormBean.getUserId());
			bannerMasterBean.setStatus("A");
			bannerDao.save(bannerMasterBean);
			result = true;

		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	public List<BannerFormBean> getBannerList() throws Exception {
		List<BannerFormBean> bannerFormBeans = new ArrayList<BannerFormBean>();

		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			List<BannerMasterBean> bannerMasterBeans = bannerDao.findBannerList();
			for (BannerMasterBean bannerMasterBean : bannerMasterBeans) {
				BannerFormBean bannerFormBean = new BannerFormBean();
				bannerFormBean.setAutoId(
						null != bannerMasterBean.getAutoId() ? bannerMasterBean.getAutoId().toString() : null);
				bannerFormBean.setUpdatedDate(
						null != bannerMasterBean.getUpdatedDate() ? dateFormat.format(bannerMasterBean.getUpdatedDate())
								: null);
				bannerFormBean.setCreatedDate(
						null != bannerMasterBean.getCreatedDate() ? dateFormat.format(bannerMasterBean.getCreatedDate())
								: null);
				bannerFormBean.setBannerContent1(bannerMasterBean.getBannerContent1());
				bannerFormBean.setBannerContent2(bannerMasterBean.getBannerContent2());
				bannerFormBean.setBannerImage(bannerMasterBean.getBannerImage());
				bannerFormBean.setBannerLinkUrl(bannerMasterBean.getBannerLinkUrl());
				bannerFormBean.setBannerOrder(bannerMasterBean.getBannerOrder());
				bannerFormBean.setBannerPosition(bannerMasterBean.getBannerPosition());
				bannerFormBean.setBannerTitle(bannerMasterBean.getBannerTitle());
				bannerFormBean.setStatus("A");
				bannerFormBeans.add(bannerFormBean);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return bannerFormBeans;
	}

	public BannerFormBean getBannerDetailsById(String autoId) throws Exception {
		BannerFormBean bannerFormBean = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			BannerMasterBean bannerMasterBean = bannerDao.findBannerById(Integer.parseInt(autoId));
			if (bannerMasterBean != null) {

				bannerFormBean = new BannerFormBean();
				bannerFormBean.setAutoId(
						null != bannerMasterBean.getAutoId() ? bannerMasterBean.getAutoId().toString() : null);
				bannerFormBean.setUpdatedDate(
						null != bannerMasterBean.getUpdatedDate() ? dateFormat.format(bannerMasterBean.getUpdatedDate())
								: null);
				bannerFormBean.setCreatedDate(
						null != bannerMasterBean.getCreatedDate() ? dateFormat.format(bannerMasterBean.getCreatedDate())
								: null);
				bannerFormBean.setBannerContent1(bannerMasterBean.getBannerContent1());
				bannerFormBean.setBannerContent2(bannerMasterBean.getBannerContent2());
				bannerFormBean.setBannerImage(bannerMasterBean.getBannerImage());
				bannerFormBean.setBannerLinkUrl(bannerMasterBean.getBannerLinkUrl());
				bannerFormBean.setBannerOrder(bannerMasterBean.getBannerOrder());
				bannerFormBean.setBannerPosition(bannerMasterBean.getBannerPosition());
				bannerFormBean.setBannerTitle(bannerMasterBean.getBannerTitle());
				bannerFormBean.setStatus(bannerMasterBean.getStatus());

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return bannerFormBean;
	}

	public Boolean deleteBannerDetailsById(String autoId) throws Exception {
		Boolean result = null;
		try {
			Integer id = bannerDao.deleteBannerById(Integer.parseInt(autoId));

			if (id != 0) {
				result = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

}
