package com.ppixel.api.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.api.bean.TestonomialFormBean;
import com.ppixel.api.bean.TestonomialMasterBean;
import com.ppixel.api.dao.TestonomialSDao;

@Service
public class TestonomialService {

	@Autowired
	TestonomialSDao testonomialSDao;

	public Boolean saveOrUpdateTestonomialDetails(TestonomialFormBean testonomialFormBean) throws Exception {
		Boolean result = false;
		try {
			TestonomialMasterBean testonomialMasterBean = new TestonomialMasterBean();
			if (testonomialFormBean.getAutoId() != null)
				testonomialMasterBean.setAutoId(testonomialFormBean.getAutoId());
			testonomialMasterBean.setAuthor(testonomialFormBean.getAuthor());
			testonomialMasterBean.setTestonomial(testonomialFormBean.getTestonomial());
			testonomialMasterBean.setCreatedDate(new Date());
			testonomialMasterBean.setPosition(testonomialFormBean.getPosition());
			testonomialMasterBean.setUpdatedDate(testonomialFormBean.getUpdatedDate());
			testonomialMasterBean.setStatus("A");
			testonomialMasterBean.setBatchId(testonomialFormBean.getBatchId());
			testonomialMasterBean.setUserId(testonomialFormBean.getUserId());
			testonomialMasterBean.setImageUrl(testonomialFormBean.getImageUrl());
			testonomialSDao.save(testonomialMasterBean);

			if (testonomialMasterBean.getAutoId() != null)
				result = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	public List<TestonomialFormBean> getAllTestonomialList() throws Exception {
		List<TestonomialFormBean> testonomialFormBeans = new ArrayList<TestonomialFormBean>();
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			List<TestonomialMasterBean> testonomialMasterBeans = testonomialSDao.getAllTestonomialList();
			for (TestonomialMasterBean testonomialMasterBean : testonomialMasterBeans) {
				TestonomialFormBean testonomialFormBean = new TestonomialFormBean();
				testonomialFormBean.setAutoId(
						null != testonomialMasterBean.getAutoId() ? testonomialMasterBean.getAutoId() : null);
				testonomialFormBean.setAuthor(
						null != testonomialMasterBean.getAuthor() ? testonomialMasterBean.getAuthor() : null);
				testonomialFormBean.setPosition(
						null != testonomialMasterBean.getPosition() ? testonomialMasterBean.getPosition() : null);
				testonomialFormBean.setTestonomial(
						null != testonomialMasterBean.getTestonomial() ? testonomialMasterBean.getTestonomial() : null);
				testonomialFormBean.setCreatedDate(
						null != testonomialMasterBean.getCreatedDate() ? testonomialMasterBean.getCreatedDate() : null);
				testonomialFormBean.setUpdatedDate(
						null != testonomialMasterBean.getUpdatedDate() ? testonomialMasterBean.getUpdatedDate() : null);
				testonomialFormBean.setBatchId(
						null != testonomialMasterBean.getBatchId() ? testonomialMasterBean.getBatchId() : null);
				testonomialFormBean.setImageUrl(
						null != testonomialMasterBean.getImageUrl() ? testonomialMasterBean.getImageUrl() : null);
				testonomialFormBean.setStatus("A");
				testonomialFormBeans.add(testonomialFormBean);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return testonomialFormBeans;
	}

	public TestonomialFormBean getTestonomialDetailsById(String autoId) throws Exception {
		TestonomialFormBean testonomialFormBean = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			TestonomialMasterBean testonomialMasterBean = testonomialSDao.getTestonomialById(Integer.parseInt(autoId));
			if (testonomialMasterBean != null) {
				testonomialFormBean = new TestonomialFormBean();
				testonomialFormBean.setAutoId(
						null != testonomialMasterBean.getAutoId() ? testonomialMasterBean.getAutoId() : null);
				testonomialFormBean.setAuthor(
						null != testonomialMasterBean.getAuthor() ? testonomialMasterBean.getAuthor() : null);
				testonomialFormBean.setPosition(
						null != testonomialMasterBean.getPosition() ? testonomialMasterBean.getPosition() : null);
				testonomialFormBean.setTestonomial(
						null != testonomialMasterBean.getTestonomial() ? testonomialMasterBean.getTestonomial() : null);
				testonomialFormBean.setCreatedDate(
						null != testonomialMasterBean.getCreatedDate() ? testonomialMasterBean.getCreatedDate() : null);
				testonomialFormBean.setUpdatedDate(
						null != testonomialMasterBean.getUpdatedDate() ? testonomialMasterBean.getUpdatedDate() : null);
				testonomialFormBean.setBatchId(
						null != testonomialMasterBean.getBatchId() ? testonomialMasterBean.getBatchId() : null);
				testonomialFormBean.setImageUrl(
						null != testonomialMasterBean.getImageUrl() ? testonomialMasterBean.getImageUrl() : null);
				testonomialFormBean.setStatus("A");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return testonomialFormBean;
	}

	public Boolean deleteTestonomialDetailsById(String autoId) throws Exception {
		Boolean result = null;
		try {
			Integer id = testonomialSDao.deleteTestonomialById(Integer.parseInt(autoId));
			if (id != 0) {
				result = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

}