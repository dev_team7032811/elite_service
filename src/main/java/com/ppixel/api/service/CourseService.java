package com.ppixel.api.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppixel.api.bean.CourseFormBean;
import com.ppixel.api.bean.CourseMasterBean;
import com.ppixel.api.dao.CourseDao;

@Service
public class CourseService {

	@Autowired
	CourseDao courseDao;

	public Boolean saveAndUpdateCourseDetails(CourseFormBean courseFormBean) throws Exception {
		Boolean result = false;
		try {
			CourseMasterBean courseMasterBean = new CourseMasterBean();
			if (courseFormBean.getAutoId() != null) {
				courseMasterBean.setAutoId(Integer.parseInt(courseFormBean.getAutoId()));
				courseMasterBean.setUpdatedDate(new Date());
			} else {
				// courseMasterBean.setCreatedDate(new Date());

			}
			courseMasterBean.setCreatedDate(new Date());
			courseMasterBean.setCourseTitle(courseFormBean.getCourseTitle());
			courseMasterBean.setDescription(courseFormBean.getDescription());
			courseMasterBean.setUserId(courseFormBean.getUserId());
			courseMasterBean.setInstructor(courseFormBean.getInstructor());
			courseMasterBean.setPrice(Double.parseDouble(courseFormBean.getPrice()));
			courseMasterBean.setStatus("A");
			courseDao.save(courseMasterBean);
			result = true;

		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	public List<CourseFormBean> getCourseList() throws Exception {
		List<CourseFormBean> courseFormBeans = new ArrayList<CourseFormBean>();

		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			List<CourseMasterBean> courseMasterBeans = courseDao.findCourseList();
			for (CourseMasterBean courseMasterBean : courseMasterBeans) {
				CourseFormBean courseFormBean = new CourseFormBean();
				courseFormBean.setAutoId(
						null != courseMasterBean.getAutoId() ? courseMasterBean.getAutoId().toString() : null);
				courseFormBean.setUpdatedDate(
						null != courseMasterBean.getUpdatedDate() ? dateFormat.format(courseMasterBean.getUpdatedDate())
								: null);
				courseFormBean.setCreatedDate(
						null != courseMasterBean.getCreatedDate() ? dateFormat.format(courseMasterBean.getCreatedDate())
								: null);
				courseFormBean.setCourseTitle(courseMasterBean.getCourseTitle());
				courseFormBean.setDescription(courseMasterBean.getDescription());
				courseFormBean.setInstructor(courseMasterBean.getInstructor());
				courseFormBean.setPrice(courseMasterBean.getPrice().toString());
				courseFormBean.setStatus("A");
				courseFormBeans.add(courseFormBean);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return courseFormBeans;
	}

	public CourseFormBean getCourseDetailsById(String autoId) throws Exception {
		CourseFormBean courseFormBean = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			CourseMasterBean courseMasterBean = courseDao.findCourseById(Integer.parseInt(autoId));
			if (courseMasterBean != null) {

				courseFormBean = new CourseFormBean();
				courseFormBean.setAutoId(
						null != courseMasterBean.getAutoId() ? courseMasterBean.getAutoId().toString() : null);
				courseFormBean.setUpdatedDate(
						null != courseMasterBean.getUpdatedDate() ? dateFormat.format(courseMasterBean.getUpdatedDate())
								: null);
				courseFormBean.setCreatedDate(
						null != courseMasterBean.getCreatedDate() ? dateFormat.format(courseMasterBean.getCreatedDate())
								: null);
				courseFormBean.setCourseTitle(courseMasterBean.getCourseTitle());
				courseFormBean.setDescription(courseMasterBean.getDescription());
				courseFormBean.setInstructor(courseMasterBean.getInstructor());
				courseFormBean.setPrice(courseMasterBean.getPrice().toString());
				courseFormBean.setStatus("A");

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return courseFormBean;
	}

	public Boolean deleteCourseDetailsById(String autoId) throws Exception {
		Boolean result = null;
		try {
			Integer id = courseDao.deleteCourseById(Integer.parseInt(autoId));

			if (id != 0) {
				result = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}
}
