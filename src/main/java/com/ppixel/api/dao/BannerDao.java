package com.ppixel.api.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ppixel.api.bean.BannerMasterBean;

@Transactional
@Repository
public interface BannerDao extends JpaRepository<BannerMasterBean, Integer> {

	@Query(" from BannerMasterBean where status='A' ")
	List<BannerMasterBean> findBannerList();

	@Query(" from BannerMasterBean where status='A' and autoId=:autoId")
	BannerMasterBean findBannerById(Integer autoId);

	@Modifying
	@Query("update BannerMasterBean m set m.status='D' where m.autoId =:autoId")
	Integer deleteBannerById(Integer autoId);
}
