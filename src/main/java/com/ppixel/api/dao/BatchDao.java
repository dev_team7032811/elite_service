package com.ppixel.api.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ppixel.api.bean.BatchMasterBean;

@Transactional
@Repository
public interface BatchDao extends JpaRepository<BatchMasterBean, Integer> {

	@Query(" from BatchMasterBean where status='A' ")
	List<BatchMasterBean> findBatchList();

	@Query(" from BatchMasterBean where status='A' and autoId=:autoId")
	BatchMasterBean findBatchById(Integer autoId);

	@Modifying
	@Query("update BatchMasterBean m set m.status='D' where m.autoId =:autoId")
	Integer deleteBatchById(Integer autoId);

}