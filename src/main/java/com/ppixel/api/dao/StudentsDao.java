package com.ppixel.api.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ppixel.api.bean.StudentsMasterBean;

@Transactional
@Repository
public interface StudentsDao extends JpaRepository<StudentsMasterBean, Integer> {

	@Query(" from StudentsMasterBean where status='A' ")
	List<StudentsMasterBean> findStudentsList();

	@Query(" from StudentsMasterBean where status='A' and autoId=:autoId")
	StudentsMasterBean findStudentsById(Integer autoId);

	@Modifying
	@Query("update StudentsMasterBean m set m.status='D' where m.autoId =:autoId")
	Integer deleteStudentsById(Integer autoId);
}
