package com.ppixel.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ppixel.api.bean.UserAdminMasterBean;

@Repository
public interface UserAdminDao extends JpaRepository<UserAdminMasterBean, Integer> {

	@Query(" from UserAdminMasterBean where status='A' and emailId=:emailId and password=:password")
	UserAdminMasterBean validateEmailAndPassword(String emailId, String password);
}
