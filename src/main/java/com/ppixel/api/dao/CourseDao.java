package com.ppixel.api.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ppixel.api.bean.CourseMasterBean;

@Transactional
@Repository
public interface CourseDao extends JpaRepository<CourseMasterBean, Integer> {

	@Query(" from CourseMasterBean where status='A' ")
	List<CourseMasterBean> findCourseList();

	@Query(" from CourseMasterBean where status='A' and autoId=:autoId")
	CourseMasterBean findCourseById(Integer autoId);
	
	@Modifying
	@Query("update CourseMasterBean m set m.status='D' where m.autoId =:autoId")
	Integer deleteCourseById(Integer autoId);
}
