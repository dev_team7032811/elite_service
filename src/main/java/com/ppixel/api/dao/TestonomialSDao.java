package com.ppixel.api.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ppixel.api.bean.TestonomialMasterBean;
@Transactional
@Repository
public interface TestonomialSDao extends JpaRepository<TestonomialMasterBean, Integer>{
	
	@Query(" from TestonomialMasterBean where status='A' ")
	List<TestonomialMasterBean> getAllTestonomialList();
	
	@Query(" from TestonomialMasterBean where status='A' and autoId=:autoId")
	TestonomialMasterBean getTestonomialById(Integer autoId);
	
	@Modifying
	@Query("update TestonomialMasterBean m set m.status='D' where m.autoId =:autoId")
	Integer deleteTestonomialById(Integer autoId);

}