package com.ppixel.api.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ppixel.api.bean.AboutusMasterBean;

@Transactional
@Repository
public interface AboutusDao extends JpaRepository<AboutusMasterBean, Integer> {

	@Query(" from AboutusMasterBean where status='A' ")
	List<AboutusMasterBean> findAboutusList();

	@Query(" from AboutusMasterBean where status='A' and autoId=:autoId")
	AboutusMasterBean findAboutusById(Integer autoId);

	@Modifying
	@Query("update AboutusMasterBean m set m.status='D' where m.autoId =:autoId")
	Integer deleteAboutusById(Integer autoId);
}
