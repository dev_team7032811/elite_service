package com.ppixel.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.wrapper.StudentsWrapper;

@RestController
public class StudentsController {

	@Autowired
	StudentsWrapper studentsWrapper;

	@PostMapping(value = "saveAndUpdateStudentsDetails")
	public ResponseDetails saveAndUpdateCourseDetails(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return studentsWrapper.saveAndUpdateStudentsDetails(requestDetails);
	}

	@PostMapping(value = "getStudentsList")
	public ResponseDetails getStudentsList(HttpServletRequest request, @RequestBody RequestDetails requestDetails) {

		return studentsWrapper.getStudentsList(requestDetails);
	}

	@PostMapping(value = "getStudentsDetailsById")
	public ResponseDetails getStudentsDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return studentsWrapper.getStudentsDetailsById(requestDetails);
	}

	@PostMapping(value = "deleteStudentsDetailsById")
	public ResponseDetails deleteCourseDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return studentsWrapper.deleteStudentsDetailsById(requestDetails);
	}

	

}
