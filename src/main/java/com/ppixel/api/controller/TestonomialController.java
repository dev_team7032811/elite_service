package com.ppixel.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.wrapper.TestonomialWrapper;

@RestController
public class TestonomialController {
	
	@Autowired
	private TestonomialWrapper testonomialWrapper;
	
	@PostMapping(value = "saveOrUpdateTestonomialDetails")
	public ResponseDetails saveOrUpdateTestonomialDetails(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return testonomialWrapper.saveOrUpdateTestonomialDetails(requestDetails);
	}

	@PostMapping(value = "getAllTestonomialDetailsList")
	public ResponseDetails getAllTestonomialDetailsList(HttpServletRequest request, @RequestBody RequestDetails requestDetails) {
		return testonomialWrapper.getAllTestonomialDetailsList(requestDetails);
	}

	@PostMapping(value = "getTestonomialDetailsById")
	public ResponseDetails getTestonomialDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return testonomialWrapper.getTestonomialDetailsById(requestDetails);
	}
	
	@PostMapping(value = "deleteTestonomialDetailsById")
	public ResponseDetails deleteTestonomialDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return testonomialWrapper.deleteTestonomialDetailsById(requestDetails);
	}

}