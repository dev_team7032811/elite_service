package com.ppixel.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.wrapper.UserAdminWrapper;

@RestController
public class UserAdminController {

	@Autowired
	UserAdminWrapper userAdminWrapper;

	@PostMapping(value = "getUserAdminDetailsById")
	public ResponseDetails getUserAdminDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {

		return userAdminWrapper.getUserAdminDetailsById(requestDetails);
	}

}
