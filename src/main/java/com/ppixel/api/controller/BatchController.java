package com.ppixel.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.wrapper.BatchWrapper;

@RestController
public class BatchController {
	
	@Autowired
	BatchWrapper batchWrapper;

	@PostMapping(value = "saveAndUpdateBatchDetails")
	public ResponseDetails saveAndUpdateBatchDetails(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return batchWrapper.saveAndUpdateBatchDetails(requestDetails);
	}

	@PostMapping(value = "getBatchList")
	public ResponseDetails getBatchList(HttpServletRequest request, @RequestBody RequestDetails requestDetails) {
		return batchWrapper.getBatchList(requestDetails);
	}

	@PostMapping(value = "getBatchDetailsById")
	public ResponseDetails getBatchDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return batchWrapper.getBatchDetailsById(requestDetails);
	}
	@PostMapping(value = "deleteBatchDetailsById")
	public ResponseDetails deleteBatchDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return batchWrapper.deleteBatchDetailsById(requestDetails);
	}

}