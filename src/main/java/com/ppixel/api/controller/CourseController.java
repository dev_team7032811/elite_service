package com.ppixel.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.wrapper.CourseWrapper;

@RestController
public class CourseController {

	@Autowired
	CourseWrapper courseWrapper;

	@PostMapping(value = "saveAndUpdateCourseDetails")
	public ResponseDetails saveAndUpdateCourseDetails(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return courseWrapper.saveAndUpdateCourseDetails(requestDetails);
	}

	@PostMapping(value = "getCourseList")
	public ResponseDetails getCourseList(HttpServletRequest request, @RequestBody RequestDetails requestDetails) {

		return courseWrapper.getCourseList(requestDetails);
	}

	@PostMapping(value = "getCourseDetailsById")
	public ResponseDetails getCourseDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return courseWrapper.getCourseDetailsById(requestDetails);
	}
	@PostMapping(value = "deleteCourseDetailsById")
	public ResponseDetails deleteCourseDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return courseWrapper.deleteCourseDetailsById(requestDetails);
	}

	@GetMapping(value = "checkAppStatus")
	public String checkAppStatus(HttpServletRequest request, @RequestBody RequestDetails requestDetails) {
		return "Application Running Successfully !!";
	}
}
