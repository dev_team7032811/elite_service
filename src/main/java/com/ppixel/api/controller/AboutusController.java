package com.ppixel.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.wrapper.AboutusWrapper;

@RestController
public class AboutusController {

	@Autowired
	AboutusWrapper aboutusWrapper;

	@PostMapping(value = "saveAndUpdateAboutus")
	public ResponseDetails saveAndUpdateAboutus(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return aboutusWrapper.saveAndUpdateAboutus(requestDetails);
	}

	@PostMapping(value = "getAboutusList")
	public ResponseDetails getAboutusList(HttpServletRequest request, @RequestBody RequestDetails requestDetails) {

		return aboutusWrapper.getAboutusList(requestDetails);
	}

	@PostMapping(value = "getAboutusById")
	public ResponseDetails getAboutusById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return aboutusWrapper.getAboutusById(requestDetails);
	}
	@PostMapping(value = "deleteAboutusById")
	public ResponseDetails deleteAboutusById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return aboutusWrapper.deleteAboutusById(requestDetails);
	}


}
