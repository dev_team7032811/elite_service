package com.ppixel.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppixel.RequestDetails;
import com.ppixel.ResponseDetails;
import com.ppixel.api.wrapper.BannerWrapper;

@RestController
public class BannerController {

	@Autowired
	BannerWrapper bannerWrapper;

	@PostMapping(value = "saveAndUpdateBannerDetails")
	public ResponseDetails saveAndUpdateBannerDetails(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return bannerWrapper.saveAndUpdateBannerDetails(requestDetails);
	}

	@PostMapping(value = "getBannerList")
	public ResponseDetails getBannerList(HttpServletRequest request, @RequestBody RequestDetails requestDetails) {

		return bannerWrapper.getBannerList(requestDetails);
	}

	@PostMapping(value = "getBannerDetailsById")
	public ResponseDetails getBannerDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return bannerWrapper.getBannerDetailsById(requestDetails);
	}

	@PostMapping(value = "deleteBannerDetailsById")
	public ResponseDetails deleteBannerDetailsById(HttpServletRequest request,
			@RequestBody RequestDetails requestDetails) {
		return bannerWrapper.deleteBannerDetailsById(requestDetails);
	}

}
