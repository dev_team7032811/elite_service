package com.ppixel;

import com.ppixel.api.bean.AboutusFormBean;
import com.ppixel.api.bean.BannerFormBean;
import com.ppixel.api.bean.BatchFormBean;
import com.ppixel.api.bean.CourseFormBean;
import com.ppixel.api.bean.StudentsFormBean;
import com.ppixel.api.bean.TestonomialFormBean;
import com.ppixel.api.bean.UserAdminFormBean;

public class RequestDetails {

	private CourseFormBean courseFormBean;
	private String id;
	private AboutusFormBean aboutusFormBean;
	private StudentsFormBean studentsFormBean;
	private BannerFormBean bannerFormBean;
	private UserAdminFormBean userAdminFormBean;
	private BatchFormBean batchFormBean;	
	private TestonomialFormBean testonomialFormBean;	

	public TestonomialFormBean getTestonomialFormBean() {
		return testonomialFormBean;
	}

	public void setTestonomialFormBean(TestonomialFormBean testonomialFormBean) {
		this.testonomialFormBean = testonomialFormBean;
	}

	public BatchFormBean getBatchFormBean() {
		return batchFormBean;
	}

	public void setBatchFormBean(BatchFormBean batchFormBean) {
		this.batchFormBean = batchFormBean;
	}

	public UserAdminFormBean getUserAdminFormBean() {
		return userAdminFormBean;
	}

	public void setUserAdminFormBean(UserAdminFormBean userAdminFormBean) {
		this.userAdminFormBean = userAdminFormBean;
	}

	public BannerFormBean getBannerFormBean() {
		return bannerFormBean;
	}

	public void setBannerFormBean(BannerFormBean bannerFormBean) {
		this.bannerFormBean = bannerFormBean;
	}

	public StudentsFormBean getStudentsFormBean() {
		return studentsFormBean;
	}

	public void setStudentsFormBean(StudentsFormBean studentsFormBean) {
		this.studentsFormBean = studentsFormBean;
	}

	public AboutusFormBean getAboutusFormBean() {
		return aboutusFormBean;
	}

	public void setAboutusFormBean(AboutusFormBean aboutusFormBean) {
		this.aboutusFormBean = aboutusFormBean;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CourseFormBean getCourseFormBean() {
		return courseFormBean;
	}

	public void setCourseFormBean(CourseFormBean courseFormBean) {
		this.courseFormBean = courseFormBean;
	}

}
