package com.ppixel;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ppixel.api.bean.AboutusFormBean;
import com.ppixel.api.bean.BannerFormBean;
import com.ppixel.api.bean.BatchFormBean;
import com.ppixel.api.bean.CourseFormBean;
import com.ppixel.api.bean.StudentsFormBean;
import com.ppixel.api.bean.TestonomialFormBean;
import com.ppixel.api.bean.UserAdminFormBean;

@JsonInclude(Include.NON_NULL)
public class ResponseDetails {

	private String responseCode;
	private String responseMessage;

	private CourseFormBean courseFormBean;
	private List<CourseFormBean> courseFormBeans;
	private AboutusFormBean aboutusFormBean;
	private List<AboutusFormBean> aboutusFormBeans;
	private List<StudentsFormBean> studentsFormBeans;
	private StudentsFormBean studentsFormBean;
	private BannerFormBean bannerFormBean;
	private List<BannerFormBean> bannerFormBeans;
	private UserAdminFormBean userAdminFormBean;
	private BatchFormBean batchFormBean;
	private List<BatchFormBean> batchFormBeans;
	private List<TestonomialFormBean> testonomialFormBeans;
	private TestonomialFormBean testonomialFormBean;
	
	public TestonomialFormBean getTestonomialFormBean() {
		return testonomialFormBean;
	}

	public void setTestonomialFormBean(TestonomialFormBean testonomialFormBean) {
		this.testonomialFormBean = testonomialFormBean;
	}

	public List<TestonomialFormBean> getTestonomialFormBeans() {
		return testonomialFormBeans;
	}

	public void setTestonomialFormBeans(List<TestonomialFormBean> testonomialFormBeans) {
		this.testonomialFormBeans = testonomialFormBeans;
	}

	public BatchFormBean getBatchFormBean() {
		return batchFormBean;
	}

	public void setBatchFormBean(BatchFormBean batchFormBean) {
		this.batchFormBean = batchFormBean;
	}

	public List<BatchFormBean> getBatchFormBeans() {
		return batchFormBeans;
	}

	public void setBatchFormBeans(List<BatchFormBean> batchFormBeans) {
		this.batchFormBeans = batchFormBeans;
	}

	public UserAdminFormBean getUserAdminFormBean() {
		return userAdminFormBean;
	}

	public void setUserAdminFormBean(UserAdminFormBean userAdminFormBean) {
		this.userAdminFormBean = userAdminFormBean;
	}

	public BannerFormBean getBannerFormBean() {
		return bannerFormBean;
	}

	public void setBannerFormBean(BannerFormBean bannerFormBean) {
		this.bannerFormBean = bannerFormBean;
	}

	public List<BannerFormBean> getBannerFormBeans() {
		return bannerFormBeans;
	}

	public void setBannerFormBeans(List<BannerFormBean> bannerFormBeans) {
		this.bannerFormBeans = bannerFormBeans;
	}

	public List<StudentsFormBean> getStudentsFormBeans() {
		return studentsFormBeans;
	}

	public void setStudentsFormBeans(List<StudentsFormBean> studentsFormBeans) {
		this.studentsFormBeans = studentsFormBeans;
	}

	public StudentsFormBean getStudentsFormBean() {
		return studentsFormBean;
	}

	public void setStudentsFormBean(StudentsFormBean studentsFormBean) {
		this.studentsFormBean = studentsFormBean;
	}

	public AboutusFormBean getAboutusFormBean() {
		return aboutusFormBean;
	}

	public void setAboutusFormBean(AboutusFormBean aboutusFormBean) {
		this.aboutusFormBean = aboutusFormBean;
	}

	public List<AboutusFormBean> getAboutusFormBeans() {
		return aboutusFormBeans;
	}

	public void setAboutusFormBeans(List<AboutusFormBean> aboutusFormBeans) {
		this.aboutusFormBeans = aboutusFormBeans;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public CourseFormBean getCourseFormBean() {
		return courseFormBean;
	}

	public void setCourseFormBean(CourseFormBean courseFormBean) {
		this.courseFormBean = courseFormBean;
	}

	public List<CourseFormBean> getCourseFormBeans() {
		return courseFormBeans;
	}

	public void setCourseFormBeans(List<CourseFormBean> courseFormBeans) {
		this.courseFormBeans = courseFormBeans;
	}

}
